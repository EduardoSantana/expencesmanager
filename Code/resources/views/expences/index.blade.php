@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-lg-12 margin-tb">
			<div class="pull-left">
				<h2>Administracion de expences</h2>
			</div>
			<div class="pull-right">
				<a class="btn btn-success" href="{{ route('expence.create') }}"> Crear Nuevo expence</a>
			</div>
		</div>
	</div>
	@if ($message = Session::get('success'))
	<div class="alert alert-success">
		<p>{{ $message }}</p>
	</div>
	@endif
	<table class="table table-bordered">
		<tr>
			<th>No</th>
			<th>Tipo</th>
			<th>Description</th>
			<th>Vencimiento</th>
			<th>Usuario</th>
			<th>Fecha</th>
			<th width="290px">Accion</th>
		</tr>
		@foreach ($data as $key => $expence)
		<tr>
			<td>{{ ++$i }}</td>
			<td>{{ $expence->type -> name }}</td>
			<td>{{ $expence->description }}</td>
			<td>{{ $expence->dueDate }}</td>
			<td>{{ $expence->user -> name }}</td>
			<td>{{ $expence->created_at }}</td>
			<td>
				<a class="btn btn-info" href="{{ route('expence.show',$expence->id) }}">Ver</a>
				<a class="btn btn-primary" href="{{ route('expence.edit',$expence->id) }}">Editar</a>
				{!! Form::open([
					'method' => 'DELETE',
					'route' => ['expence.destroy', $expence->id],
					'style'=>'display:inline'
					]) !!}
				{!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
				{!! Form::close() !!}
			</td>
		</tr>
		@endforeach
	</table>
	{!! $data->render() !!}
</div>
@endsection
