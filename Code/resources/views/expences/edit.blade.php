@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-lg-12 margin-tb">
			<div class="pull-left">
				<h2>Editar Expence</h2>
			</div>
			<div class="pull-right">
				<a class="btn btn-primary" href="{{ route('expence.index') }}"> Atras</a>
			</div>
		</div>
	</div>
	@if (count($errors) > 0)
	<div class="alert alert-danger">
		<strong>Whoops!</strong> There were some problems with your input.<br><br>
		<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
	@endif
	{!! Form::model($expence, ['method' => 'PATCH','route' => ['expences.update', $expence->id]]) !!}
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Tipo:</strong>
				{!! Form::select('typeId', $expenceTypes, null, array('placeholder' => 'Tipo','class' => 'form-control')) !!}
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Vencimiento:</strong>
				{!! Form::date('dueDate', \Carbon\Carbon::now(), array('placeholder' => 'Vencimiento','class' => 'form-control')) !!}
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Descripcion:</strong>
				{!! Form::number('amount', null, array('placeholder' => 'Amount','class' => 'form-control')) !!}
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Descripcion:</strong>
				{!! Form::textarea('description', null, array('placeholder' => 'Descripcion','class' => 'form-control')) !!}
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Notas:</strong>
				{!! Form::textarea('notes', null, array('placeholder' => 'Notas','class' => 'form-control')) !!}
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 text-center">
			<button type="submit" class="btn btn-primary">Guardar</button>
		</div>
	</div>
	{!! Form::close() !!}
</div>
@endsection