@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-lg-12 margin-tb">
			<div class="pull-left">
				<h2>Editar Tenant</h2>
			</div>
			<div class="pull-right">
				<a class="btn btn-primary" href="{{ route('tenants.index') }}"> Atras</a>
			</div>
		</div>
	</div>
	@if (count($errors) > 0)
	<div class="alert alert-danger">
		<strong>Whoops!</strong> There were some problems with your input.<br><br>
		<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
	@endif
	{!! Form::model($tenant, ['method' => 'PATCH','route' => ['tenants.update', $tenant->id]]) !!}
	<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="form-group">
				<strong>Nombre:</strong>
				{!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Description:</strong>
				{!! Form::text('description', null, array('placeholder' => 'description','class' => 'form-control')) !!}
			</div>
		</div>
		
		<div class="col-xs-12 col-sm-12 col-md-12 text-center">
			<button type="submit" class="btn btn-primary">Guardar</button>
		</div>
	</div>
	{!! Form::close() !!}
</div>
@endsection