@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-lg-12 margin-tb">
			<div class="pull-left">
				<h2>Administracion de tenants</h2>
			</div>
			<div class="pull-right">
				<a class="btn btn-success" href="{{ route('tenants.create') }}"> Crear Nuevo tenant</a>
			</div>
		</div>
	</div>
	@if ($message = Session::get('success'))
	<div class="alert alert-success">
		<p>{{ $message }}</p>
	</div>
	@endif
	<table class="table table-bordered">
		<tr>
			<th>No</th>
			<th>Name</th>
			<th>Description</th>
			<th width="290px">Accion</th>
		</tr>
		@foreach ($data as $key => $tenant)
		<tr>
			<td>{{ ++$i }}</td>
			<td>{{ $tenant->name }}</td>
			<td>{{ $tenant->description }}</td>
			<td>
				<a class="btn btn-info" href="{{ route('tenants.show',$tenant->id) }}">Ver</a>
				<a class="btn btn-primary" href="{{ route('tenants.edit',$tenant->id) }}">Editar</a>
				{!! Form::open([
					'method' => 'DELETE',
					'route' => ['tenants.destroy', $tenant->id],
					'style'=>'display:inline'
					]) !!}
				{!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
				{!! Form::close() !!}
			</td>
		</tr>
		@endforeach
	</table>
	{!! $data->render() !!}
</div>
@endsection