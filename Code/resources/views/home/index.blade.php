@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Dashboard de Expences
                </div>
                <div class="panel-body dash-panel">
                    @php
                        $tieneResultados = false;
                    @endphp
                    @forelse($listado as $i)
                        @php
                            $tieneResultados = true;
                        @endphp
                        @if ($i -> amount > 999)
                              <div class="bg-info">
                                  <div class="bs-callout bs-callout-info">
									<h4 class="pull-right">{{ $i -> description }}</h4>
									<h4>
										<i class="fa fa-smile-o" aria-hidden="true"></i>
										@php
											$fechaTomado = new DateTime($i -> created_at);
										@endphp
										Created on {{ $fechaTomado -> format('Y-m-d') }} at {{ $fechaTomado -> format('H:i') }} hours
									</h4>
									<div class="col-md-6">
										Notes: {{ $i -> notes }}
									</div>
									<div class="col-md-6">
										Amount: {{ $i -> amount }}
									</div>
									<div class="col-md-6">
										{{ $i -> dueDate }}
									</div>
									<div class="col-md-6">
										Good Job Boy!
									</div>
									<div class="clearfix"></div>
								</div>
                            </div>
                        @else
                            <div class="bg-danger">
                                <div class="bs-callout bs-callout-warning">
								<h4 class="pull-right">{{ $i -> description }}</h4>
									<h4>
									<i class="fa fa-meh-o" aria-hidden="true"></i>
									@php
										$fechaTomado = new DateTime($i -> created_at);
									@endphp
									Created on {{ $fechaTomado -> format('Y-m-d') }} at {{ $fechaTomado -> format('H:i') }} hours
									</h4>
									<div class="col-md-6">
                                        Notes: {{ $i -> notes }}
									</div>
									<div class="col-md-6">
										Amount: {{ $i -> amount }}
									</div>
									<div class="col-md-6">
										{{ $i -> dueDate }}
									</div>
									<div class="col-md-6">
										Better luck next time!
									</div>
									<div class="clearfix"></div>
								</div>
                            </div>
                        @endif
                    @empty
                        <div class="col-md-12">
                          <div style="text-align: center;">
                            <a href="{{ route('expence.create') }}" class="btn btn-success btn-lg">Registrar un Expence!</a>
                          </div>
                        </div>
                    @endforelse
                    @if ($tieneResultados == true)
                        <div class="col-md-12">
                          <div style="text-align: center;">
                            <a href="{{ route('expence.create') }}" class="btn btn-success btn-lg">Nuevo Expence!</a>
                          </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ url('/js/home/index.js') }}"></script>
@endsection