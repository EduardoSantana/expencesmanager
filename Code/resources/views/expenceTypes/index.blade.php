@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-lg-12 margin-tb">
			<div class="pull-left">
				<h2>Administracion de Expence Types</h2>
			</div>
			<div class="pull-right">
				<a class="btn btn-success" href="{{ route('expenceTypes.create') }}"> Crear Nuevo Expence Type</a>
			</div>
		</div>
	</div>
	@if ($message = Session::get('success'))
	<div class="alert alert-success">
		<p>{{ $message }}</p>
	</div>
	@endif
	<table class="table table-bordered">
		<tr>
			<th>No</th>
			<th>Name</th>
			<th width="290px">Accion</th>
		</tr>
		@foreach ($data as $key => $expenceType)
		<tr>
			<td>{{ ++$i }}</td>
			<td>{{ $expenceType->name }}</td>
			<td>
				<a class="btn btn-info" href="{{ route('expenceTypes.show',$expenceType->id) }}">Ver</a>
				<a class="btn btn-primary" href="{{ route('expenceTypes.edit',$expenceType->id) }}">Editar</a>
				{!! Form::open([
					'method' => 'DELETE',
					'route' => ['expenceTypes.destroy', $expenceType->id],
					'style'=>'display:inline'
					]) !!}
				{!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
				{!! Form::close() !!}
			</td>
		</tr>
		@endforeach
	</table>
	{!! $data->render() !!}
</div>
@endsection