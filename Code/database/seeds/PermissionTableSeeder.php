<?php

use Illuminate\Database\Seeder;
use App\Permission;

class PermissionTableSeeder extends Seeder
{
	/**
     * Run the database seeds.
	* composer dump-autoload
     * php artisan db:seed --class=PermissionTableSeeder
     * @return void
     */
	public function run()
	{
		$permission = [
			[
				'name' => 'admin-admin',
				'display_name' => 'Administrador de aplicacion',
				'description' => 'Administrador de la aplicacion'
			],
			[
				'name' => 'ver-examen',
				'display_name' => 'Ver propio examen',
				'description' => 'Ver mi propio examen'
			],
			[
				'name' => 'ver-examen-todos',
				'display_name' => 'Ver todos los examenes',
				'description' => 'Ver todos los examenes'
			],
			[
				'name' => 'ver-examenes-crear',
				'display_name' => 'Ver todos los examenes',
				'description' => 'Ver todos los examenes'
			],
			[
				'name' => 'ver-usuarios',
				'display_name' => 'Ver todos los usuarios',
				'description' => 'Ver todos los usuarios'
			],
			[
				'name' => 'ver-roles',
				'display_name' => 'Ver todos los roles',
				'description' => 'Ver todos los roles'
			],
			[
				'name' => 'ver-tomar',
				'display_name' => 'Ver tomar examen',
				'description' => 'Ver tomar examen'
			],
			[
				'name' => 'ver-tomados',
				'display_name' => 'Ver examenes tomados',
				'description' => 'Ver examenes tomados'
			]
		];

		foreach ($permission as $key => $value) {
			Permission::create($value);
		}
	}
}