<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpenceTypes extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'name'
	];

	protected $table = "expenceTypes";

}