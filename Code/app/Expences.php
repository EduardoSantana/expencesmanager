<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expences extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = 
	[
		'name',
		'typeId',
		'description',
		'notes',
		'dueDate',
		'tenantId',
		'userId',
		'amount'
	];

	protected $table = "expences";

	public function type()
    {
        return $this->hasOne('App\ExpenceTypes', 'id', 'typeId');
	}
	
	public function user()
    {
        return $this->hasOne('App\User', 'id', 'userId');
	}
	
	public function tenant()
    {
        return $this->hasOne('App\Tenants', 'id', 'tenantId');
    }

}