<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ExpenceTypes;
use DB;
use Hash;
use Mail;
use Auth;
use Redirect;

class ExpenceTypesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function index(Request $request)
	{
		$data = ExpenceTypes::orderBy('id','DESC')->paginate(5);
		return view('expenceTypes.index',compact('data'))
			->with('i', ($request->input('page', 1) - 1) * 5);
	}

	/**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function create()
	{
		return view('expenceTypes.create');
	}

	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	public function store(Request $request)
	{
		$this->validate($request, [
			'name' => 'required'
		]);

		$input = $request->all();

		$expenceType = ExpenceTypes::create($input);

		return redirect()->route('expenceTypes.index')
			->with('success','Expence Types created successfully');
	}

	/**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	public function show($id)
	{
		$expenceType = ExpenceTypes::find($id);
		return view('expenceTypes.show',compact('expenceType'));
	}

	/**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	public function edit($id)
	{
		$expenceType = ExpenceTypes::find($id);
		return view('expenceTypes.edit',compact('expenceType'));
	}

	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			'name' => 'required'
		]);

		$input = $request->all();
		$expenceType = ExpenceTypes::find($id);
		$expenceType->update($input);

		return redirect()->route('expenceTypes.index')
			->with('success','ExpenceTypes updated successfully');
	}

	/**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	public function destroy($id)
	{
		ExpenceTypes::find($id)->delete();
		return redirect()->route('expenceTypes.index')
			->with('success','ExpenceTypes deleted successfully');
	}
}