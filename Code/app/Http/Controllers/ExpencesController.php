<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Expences;
use App\ExpenceTypes;
use App\User;
use DB;
use Hash;
use Mail;
use Auth;
use Redirect;

class ExpencesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function index(Request $request)
	{
		$tenantId = User::find(Auth::id())
			 	-> tenants()
			 	-> first()
			 	-> id;

		$data = Expences::where('tenantId',$tenantId)->orderBy('id','DESC')->paginate(5);
		return view('expences.index',compact('data'))
			->with('i', ($request->input('page', 1) - 1) * 5);
	}

	/**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function create()
	{
		$expenceTypes = ExpenceTypes::orderBy('id','DESC')->lists('name','id');
		return view('expences.create',compact('expenceTypes'));
	}

	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	public function store(Request $request)
	{
		$this->validate($request, [
			'typeId' => 'required',
			'description' => 'required',
			'amount' => 'required'
		]);

		$userId = Auth::id();
		$tenantId = User::find($userId)
			 	-> tenants()
			 	-> first()
			 	-> id;
		
		$input = $request->all();
		$input['userId'] = $userId;
		$input['tenantId'] = $tenantId;

		$expence = Expences::create($input);

		return redirect()
			->route('expence.index')
			->with('success','Expences created successfully');
	}

	/**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	public function show($id)
	{
		$expence = Expences::find($id);
		return view('expences.show',compact('expence'));
	}

	/**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	public function edit($id)
	{
		$expence = Expences::find($id);
		$expenceTypes = ExpenceTypes::orderBy('id','DESC')->lists('name','id');
		return view('expences.edit',compact('expence','expenceTypes'));
	}

	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			'typeId' => 'required',
			'description' => 'required',
			'amount' => 'required'
		]);

		$input = $request->all();
		$expence = Expences::find($id);
		$expence->update($input);

		return redirect()
			->route('expence.index')
			->with('success','Expences updated successfully');
	}

	/**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	public function destroy($id)
	{
		Expences::find($id)->delete();
		return redirect()
			->route('expence.index')
			->with('success','Expences deleted successfully');
	}
}