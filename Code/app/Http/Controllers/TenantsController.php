<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tenants;
use DB;
use Hash;
use Mail;
use Auth;
use Redirect;

class TenantsController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function index(Request $request)
	{
		$data = Tenants::orderBy('id','DESC')->paginate(5);
		return view('tenants.index',compact('data'))
			->with('i', ($request->input('page', 1) - 1) * 5);
	}

	/**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function create()
	{
		return view('tenants.create');
	}

	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	public function store(Request $request)
	{
		$this->validate($request, [
			'name' => 'required'
		]);

		$input = $request->all();

		$tenant = Tenants::create($input);

		return redirect()->route('tenants.index')
			->with('success','Tenants created successfully');
	}

	/**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	public function show($id)
	{
		$tenant = Tenants::find($id);
		return view('tenants.show',compact('tenant'));
	}

	/**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	public function edit($id)
	{
		$tenant = Tenants::find($id);
		return view('tenants.edit',compact('tenant'));
	}

	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			'name' => 'required'
		]);

		$input = $request->all();
		$tenant = Tenants::find($id);
		$tenant->update($input);

		return redirect()->route('tenants.index')
			->with('success','Tenants updated successfully');
	}

	/**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	public function destroy($id)
	{
		Tenants::find($id)->delete();
		return redirect()->route('tenants.index')
			->with('success','Tenants deleted successfully');
	}
}