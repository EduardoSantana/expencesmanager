<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Expences;
use App\User;
use Auth;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listado = Expences::where('tenantId', $this->CurrentTenantId()) -> latest() -> take(6) -> get();
        return view('home.index', compact('listado'));
    }

}
