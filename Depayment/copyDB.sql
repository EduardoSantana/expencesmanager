-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: dbExpencesMan
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

use expences_manager;
--
-- Table structure for table `expenceTypes`
--

DROP TABLE IF EXISTS `expenceTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `expenceTypes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `expenceTypes`
--

LOCK TABLES `expenceTypes` WRITE;
/*!40000 ALTER TABLE `expenceTypes` DISABLE KEYS */;
INSERT INTO `expenceTypes` VALUES (1,'Gastos','2018-12-08 17:25:15','2018-12-08 17:25:15'),(2,'Prestamos','2018-12-08 17:25:24','2018-12-08 17:25:24'),(3,'Fiestas','2018-12-08 17:25:34','2018-12-08 17:25:34'),(4,'asdfadsfasdf','2018-12-11 15:57:40','2018-12-11 15:57:40');
/*!40000 ALTER TABLE `expenceTypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `expences`
--

DROP TABLE IF EXISTS `expences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `expences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `typeId` int(10) unsigned NOT NULL,
  `description` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `notes` varchar(1500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dueDate` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tenantId` int(10) unsigned NOT NULL,
  `userId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tenantId` (`tenantId`),
  KEY `typeId` (`typeId`),
  KEY `userId` (`userId`),
  CONSTRAINT `expences_ibfk_1` FOREIGN KEY (`tenantId`) REFERENCES `tenants` (`id`),
  CONSTRAINT `expences_ibfk_2` FOREIGN KEY (`typeId`) REFERENCES `expenceTypes` (`id`),
  CONSTRAINT `expences_ibfk_3` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `expences`
--

LOCK TABLES `expences` WRITE;
/*!40000 ALTER TABLE `expences` DISABLE KEYS */;
INSERT INTO `expences` VALUES (1,2,'Hoy es un dia bello asdfasdf','Siiiiiiiiiiiiiiisdfasdfasdf','2018-12-11 00:00:00','2018-12-11 15:09:02','2018-12-11 15:57:17',1,1),(2,2,'asdfasdfasdf','asdfasdfasdf','2018-12-11 00:00:00','2018-12-11 15:19:59','2018-12-11 15:19:59',1,1);
/*!40000 ALTER TABLE `expences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2017_04_07_012857_create_tomarexamendetalles_table',1),('2017_04_07_180512_create_tomarexamen_table',1),('2017_04_07_181012_create_examenes_table ',1),('2017_04_07_191012_create_preguntas_table',1),('2017_04_07_201012_create_opciones_table',1),('2017_04_28_201013_create_acl_tables',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,4),(2,4),(3,4),(4,4),(5,4),(6,4),(7,4),(9,4),(9,5);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'admin-admin','Administrador de aplicacion','Administrador de la aplicacion','2017-04-30 22:43:50','2017-04-30 22:43:50'),(2,'view-tenants','View Tenants','View Tenants for display','2018-12-11 22:43:50','2018-12-11 22:43:50'),(3,'view-expences','View Expences','View Expences for display','2018-12-11 22:43:50','2018-12-11 22:43:50'),(4,'view-expenceType','View Expence Type','View Expence Types for display','2018-12-11 22:43:50','2018-12-11 22:43:50'),(5,'ver-usuarios','Ver usuarios','Ver todos los usuarios','2017-04-30 22:43:50','2017-04-30 22:43:50'),(6,'ver-roles','Ver roles','Ver todos los roles','2017-04-30 22:43:50','2017-04-30 22:43:50'),(7,'edit-my-user','Edit my own user','Can edit my own user','2018-12-11 22:43:50','2018-12-11 22:43:50'),(9,'editar-usuario','Editar usuario','Puede editar su propio usuario','2017-04-30 22:43:50','2017-04-30 22:43:50');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_user_role_id_foreign` (`role_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` VALUES (1,4),(6,5),(14,5),(15,5),(16,5),(17,5),(18,5),(19,5),(20,5),(21,5),(22,5),(23,5),(24,5),(25,5),(26,5),(27,5),(28,5),(29,5),(30,5),(31,5);
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (4,'administrador','Administrador','Administrador de aplicacion ','2017-04-30 23:58:45','2017-05-01 16:51:44'),(5,'nomal-user','Normal User','Normal user','2017-05-01 00:33:11','2017-05-01 16:52:20');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tenants`
--

DROP TABLE IF EXISTS `tenants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tenants` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tenants`
--

LOCK TABLES `tenants` WRITE;
/*!40000 ALTER TABLE `tenants` DISABLE KEYS */;
INSERT INTO `tenants` VALUES (1,'Compania 1',NULL,NULL,NULL),(2,'Promovido','asdf','2018-12-08 16:57:46',NULL),(4,'asdfasdfasdf',NULL,'2018-12-11 16:27:01','2018-12-11 16:27:01');
/*!40000 ALTER TABLE `tenants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tenants_users`
--

DROP TABLE IF EXISTS `tenants_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tenants_users` (
  `tenantId` int(10) unsigned NOT NULL,
  `userId` int(10) unsigned NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`tenantId`,`userId`),
  KEY `userId` (`userId`),
  KEY `tenantId` (`tenantId`),
  CONSTRAINT `tenants_users_ibfk_1` FOREIGN KEY (`tenantId`) REFERENCES `tenants` (`id`),
  CONSTRAINT `tenants_users_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tenants_users`
--

LOCK TABLES `tenants_users` WRITE;
/*!40000 ALTER TABLE `tenants_users` DISABLE KEYS */;
INSERT INTO `tenants_users` VALUES (1,1,NULL,NULL),(1,29,NULL,NULL),(2,30,NULL,NULL),(4,31,NULL,NULL);
/*!40000 ALTER TABLE `tenants_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rolId` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `Activo` tinyint(1) NOT NULL DEFAULT '1',
  `nopassword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Eduardo Amparo Santana Severino','eduardo_amparo@hotmail.com','$2y$10$Vboj1u/zu6uoEYjscMX8k.IdsPwozUIN1sIRyuaOs.cwcHAXMtp4W','O6Ddj3rRsGlR0m2aNfAhqN3D8MYn8ASEbskpeHfXi7OZCEt6ba4DWlLHX8Mm',1,'2017-04-08 03:01:49','2017-05-22 06:42:35',1,'1234567890'),(6,'Eduardo  2 Amparo Normal','eduardoamparos@gmail.com','$2y$10$epH9ccaCjWEnUfi8YcB1GeTrC5FDMpMPSHcp.qNqSjWWdQBPCfB7C','5shAo7K9ItACpiuNvqbZuEfcDXqJLLiegiVqG6usRUxrEv2kUjs9iFukY4rg',3,'2017-04-22 15:17:00','2017-11-30 06:28:49',1,'1234567890'),(14,'Amaury Esteban Nunez Santos','amaurynunez@gmail.com','$2y$10$Yv0ROa.9MoVdSorp8.F0U.MBPRzYpw.U5ma/60PbaT08w5wSnV.i.','0kz8FebKNFqh5PuurZQu6pviyqufveErxqnI9kUc6OM0LPICcXSf2brRBEmK',0,'2017-05-03 08:42:36','2018-07-16 18:22:13',1,'1234567890'),(15,'Max Luzon','maxluzon@gmail.com','$2y$10$.JnzVjgOCg2OnM0Ggil6qufajCf5xmq8r0w85eMhCmsZuxR4OrDVC','0wkSIp1uJyrmPZx0PandxAvrZCA0A9E00ce1yJbgwtocTkFHpGdNpAS22zFh',0,'2017-05-08 03:06:42','2017-05-08 03:17:30',1,'123456789'),(16,'Edward Vinas Rosa','edwardvinas7@gmail.com','$2y$10$weM5hUAJuS7EEJ/SV5zJaOf81Wh2MOnjouMytzqJo975Mx9Oy0GSS','awCrq04T4PInQpZByLCCGJfDtMnSdWTtCGe16XVoOMr6tvNPJpgnPhz8j22p',0,'2017-05-22 07:54:47','2017-08-30 00:00:56',1,'edwardvinas7'),(17,'Elias Jimenez','elias-j-r@hotmail.com','$2y$10$djPpH5VCJI484YKyYbZ6PuESlqBpNUOYKo3Zy0ArfMQfhlKahnRDq','GXNTJ4mkrdf907x2EfCUr6SGL8HtRpiEnCwiXSieaWogsbvNhuytWf657kQj',0,'2017-05-24 09:01:26','2017-05-24 09:03:47',1,'elias21'),(18,'Leonel Feliz','Inglfeliz@gmail.com','$2y$10$pi317UyZNgpJ8fbF3z2bR.mx9C.zHMXWkbhpZFgq.azsa8QHM9.i.','9KYlgYggGV94FzkE0Gms24X27qi16bfuBUUHIxURxuWsVzMQv7U5c4OwJsx5',0,'2017-05-30 21:21:04','2017-05-30 23:35:55',1,'12345678'),(19,'Cesar Delgado Diaz','Cesar.delgado.diaz@gmail.com','$2y$10$f6NRAW5Evhz5.xuUE7530eOwyG2ykt9d/35fR1wKgTnmlCvs6rK0m','kGcNGX8PvuIf4foY5sqmyu7enjdgniA0TBTNesEInTgtYy9K2gOBw29VGW9H',0,'2017-06-09 06:22:53','2017-06-14 09:49:59',1,'cesar1234'),(20,'Danny Hilario Suarez','Dannyhilariosuarez@gmail.com','$2y$10$6VdT97HNZn1Dl6Uwp2EqiOm3jgTwq2AE6.5uAyWVniUPkVqcY30Cm','H6Ho55eExNNCi2piL2RSsEWpamuifpI4Y2tTOcTBfkW8ZnyOP06rB9uSosIJ',0,'2017-06-09 08:34:08','2017-06-10 01:58:16',1,'danny1234'),(21,'Norman Carrasco','norman.carrasco@hotmail.com','$2y$10$rNknwRuVNdroYrJfRaHJ/O159KNEiJyt9R6qLeiKMZXYqMji57xDu','I28ET2flVWijvwS4mCCNHbEV6kmRaHvjF1Q7tpanYokrYdKp3MSGdvPflh95',0,'2017-06-09 08:41:31','2017-06-09 08:48:50',1,'norman1234'),(22,'Isargenys ','Isargenys@gmail.com','$2y$10$QqpBisFEK1EJFbA.lPPqou513YRp05a.M/FGJBnW7CrOSA4yBAEka','eQ7mcpOBtPtLoutLrcCxJHMxq1e3RzUYHbvKQR4lnmyUuM0uWt3sgzMlDvID',0,'2017-06-10 09:32:41','2017-06-10 09:56:55',1,'isa1234'),(23,'Alicia Acevedo ','Acevedo.alicris@gmail.com','$2y$10$bjFkw1JqkDWCNiJlbGb59O2jx1KX9kXa1pGdRQeGvhDDNwRCUJLhO','FCrXHptrXw1y894ltvdJzRBxow4DMTqgOKtlxpviYyXrtZKsclTw3f0TwIZh',0,'2017-07-05 21:03:43','2017-08-06 21:01:19',1,'123'),(24,'Manuel Vanderhost','tavlita@gmail.com','$2y$10$M4u3IeqXur22g4.l76qEzeCcTUDQlRDAEL9X7QCkDTnDl6705zVMK',NULL,0,'2017-08-27 08:25:51','2017-09-19 19:24:53',1,'123456789'),(25,'Jose Manzanillo ','joframare@gmail.com','$2y$10$SABUBhEjMuR./lnJcNWvfuEl23.T8.XltGOLYsL1ud4/osntAN9BK','XA5ShkVvy8tN71dmGwn3SWYv8vqGySdGuJMyUxVrPcqgFYQ0RHenazmiw1nN',0,'2017-10-08 21:05:49','2017-10-09 00:05:20',1,'123456789'),(26,'Amaury Gomez','amaurygomez1993@gmail.com','$2y$10$boZh2w.PFNOHgTpe1jSbEur0mR1p5xvnLqId5kiUQPPOL0Jt61bNu','hLF81HR9F4j6lx4VZVrRy1gYF8EuhcJsPZI6LdVvjGmd2eHQt0DCUQDrXsw2',0,'2017-11-03 04:01:29','2017-11-03 18:40:01',1,'123456789'),(27,'Jose Osmil Ruiz Dionicio','Joseosmilruiz@gmail.com','$2y$10$nhQ6qBKGfEyzbAt4DNdcXuAMHo5ZjZHOpTo.Q1msFsIfEPAlscVlq','UUKzSFS77dcGMcZzSatDcfCDZ2fEXBmDIqIW5lqqLBCYcQ0m0NHcdIOmzxLT',0,'2018-05-08 22:37:09','2018-05-08 23:10:02',1,'123456789'),(28,'Josias Riveron','josiasriveron@gmail.com','$2y$10$lXbytfBL.Tzj0.D9oxiB6eV5nGLwoEy88jka8TUh.A226E0MaDM26','nirkUedx3OneMvcG6d065XklYIujoHJOGJLDuZCm6IsO4CZIaTPcSyHMYa6M',0,'2018-06-30 00:00:51','2018-06-30 00:04:05',1,'123456789'),(29,'ramona','me@me.com','$2y$10$QcBQVBDka0dDfwJcSemqceThUTScNcr0VfsTuHNXhP8voBt1mIRGK',NULL,0,'2018-12-11 16:15:53','2018-12-11 16:15:53',1,'123456789'),(30,'asdf','me@me2.com','$2y$10$LcavId0l7OZ.rm8VvLyBzuRRtHotn3yNGTymHOKNlUdTvt2yG8C3i',NULL,0,'2018-12-11 16:25:29','2018-12-11 16:25:29',1,'1234567890'),(31,'asdfasdfasdf','me3@me2.com','$2y$10$D23Ro1n66FVGQ02REgLTMercGXiWpcGofDRZt.b0DHTZFL3xILNv.',NULL,0,'2018-12-11 16:27:01','2018-12-11 16:27:01',1,'123456789');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-11 17:59:36
